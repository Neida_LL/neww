export interface UsuarioI {
  usuario:String;
  password:string;
}

export interface UsuarioDataI {
  id: string;
  nombres: string;
  apellidos: string;
  email :string;
  celular: number;
  fecha:string;
  hora:string;
  descripcion:string;
}
