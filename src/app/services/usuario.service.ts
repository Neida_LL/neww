
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { UsuarioDataI } from 'src/app/interface/usuario.interface';


@Injectable({
  providedIn: 'root'
})



export class UsuarioService {

  listUsuarios: UsuarioDataI[] = [];

  

  constructor(private router:Router) {
    this.cargarDatos();
   }

  getUsuario(): UsuarioDataI[] {
    //slice retorna  una copia del array
    return this.listUsuarios.slice();
  }


  eliminarUsuario(usuario:any){
    this.cargarDatos();
    this.listUsuarios =this.listUsuarios.filter(data => {
      return data.id.toString() !== usuario.toString(); 
    })
    localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
    this.cargarDatos();
  }

  agregarUsuario(usuario:UsuarioDataI){

    this.listUsuarios.unshift(usuario);
    localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
    this.cargarDatos();

  }

   buscarUsuario(id: any): UsuarioDataI{
     //o retorna un json {} vacio
     return this.listUsuarios.find(element => element.id == id) || {} as UsuarioDataI;
   }


  modificarUsuario(user: UsuarioDataI){
    this.cargarDatos();
    this.eliminarUsuario(user.id);
    this.agregarUsuario(user);
  }

  cargarDatos(){
    if (!localStorage.getItem("listaAgenda")) {

      console.log(this.listUsuarios);
      localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaAgenda');
      this.listUsuarios = JSON.parse(guardados || '{}');
      console.log(this.listUsuarios[0]);

    } else{
      
      //localStorage.setItem("listaAgenda", JSON.stringify( this.listUsuarios));
      let guardados = localStorage.getItem('listaAgenda');
      this.listUsuarios = JSON.parse(guardados || '{}');
    }
  }

}
