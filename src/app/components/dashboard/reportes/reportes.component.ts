import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';

@Component({
  selector: 'app-reportes',
  templateUrl: './reportes.component.html',
  styleUrls: ['./reportes.component.css']
})
export class ReportesComponent implements OnInit {

  form!:FormGroup
  loading:boolean = false;
  constructor(private fb:FormBuilder, private _snackbar: MatSnackBar, private router:Router ) { 
  this.formulario();
  }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      nombre:['',[Validators.required,Validators.pattern(/^[a-zA-ZñÑ\s]+$/)]],
      email:['',[Validators.required]],
      mensaje:['',[Validators.required,Validators.pattern(/^[0-9a-zA-ZñÑ\s]+$/)]],
    })
  }

  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading=false;
    //Redireccionamos al dashboard
      //this.router.navigate(['reportes']);
      this.form.reset();
      this._snackbar.open('Mensaje enviado exitosamente','',{
        duration:5000,
        horizontalPosition:'center',
        verticalPosition:'bottom'
      })
    }, 1500);
  }

   ingresar(){
     console.log(this.form.value);
     

     if (this.form.valid) {
       //Redireccionamos al dashboard
       this.fakeLoading();
       
     } else {
       //mostramos un mensaje de error
       this.error();
       this.form.reset();
     }
  
   }


  error():void{
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }

}
