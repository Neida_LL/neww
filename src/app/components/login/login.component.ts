
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioI } from 'src/app/interface/usuario.interface';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  form!:FormGroup
  loading:boolean = false;
  constructor(private fb:FormBuilder, private _snackbar: MatSnackBar, private router:Router ) { 
  this.formulario();
  }

  ngOnInit(): void {
  }

  formulario():void{
    this.form=this.fb.group({
      usuario:['',Validators.required],
      password:['',Validators.required]
    })
  }

  fakeLoading():void{
    this.loading = true;
    setTimeout(() => {
      this.loading=false;
    //Redireccionamos al dashboard
      this.router.navigate(['dashboard'])      
    }, 1500);
  }

  ingresar(){
    console.log(this.form.value);
    const Usuario:UsuarioI ={
      usuario: this.form.value.usuario,
      password: this.form.value.password
    }    
    console.log(Usuario);

    if (Usuario.usuario==='jperez' && Usuario.password==='admin123') {
      //Redireccionamos al dashboard
      this.fakeLoading();
    } else {
      //mostramos un mensaje de error
      this.error();
      this.form.reset();
    }
  
  }


  error():void{
    this._snackbar.open('usuario o contraseña incorrecto','',{
      duration:5000,
      horizontalPosition:'center',
      verticalPosition:'bottom'
    })
  }

}
